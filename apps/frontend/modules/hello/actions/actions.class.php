<?php

/**
 * hello actions.
 *
 * @package    jobeet
 * @subpackage hello
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class helloActions extends sfActions {
    /**
     * Executes index action
     *
     * @param sfRequest $request A request object
     */
    public function executeIndex(sfWebRequest $request) {
        $this->forward('default', 'module');
    }
    public function executeWorld() {
        $this->name = "world<script>alert('xss');</script>>ddd";
        $this->hoge = new Hoge();
        $this->hoge->name = "hogeeee<script>alert('hogeeeee11');</script>!!";

    }

    public function executeError() {

        return sfView::ERROR;
    }
}
class Hoge {
    public $name;
    public function getName() {
        return $this->name;
    }
}
